; Drush Make (http://drupal.org/project/drush_make)
api = 2
core = 7.x

; Additional dependecies.

projects[coffee] = 1.0

projects[conduit][type] = module
projects[conduit][download][type] = git
projects[conduit][download][url] = http://git.drupal.org/project/conduit.git
projects[conduit][download][branch] = 7.x-1.x

projects[conduit_drupal][type] = module
projects[conduit_drupal][download][type] = git
projects[conduit_drupal][download][url] = http://git.drupal.org/project/conduit_drupal.git
projects[conduit_drupal][download][branch] = 7.x-1.x

projects[ctools] = 1.2

projects[environment][type] = module
projects[environment][download][type] = git
projects[environment][download][url] = git://github.com/boombatower/environment.git
projects[environment][download][branch] = 7.x-1.x

projects[environment_indicator] = 1.1
projects[features] = 1.0-rc1
projects[markdown] = 1.0
projects[module_filter] = 1.7
projects[pathauto] = 1.2
projects[strongarm] = 2.0-rc1
projects[token] = 1.4
